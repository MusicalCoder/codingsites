import sys
import os

def parse(input):
    """Parse puzzle input"""
    new_list = []
    for line in input:
        qty, value, pwd = line.split(" ")
        new_list.append([qty, value, pwd])
    
    return new_list


def calc_valid_passwords(input):
    valid_count = 0

    for entry in input:
        low, high = map(int,entry[0].split("-"))
        letter = entry[1][0]
        password = entry[2]

        letter_count = password.count(letter)

        if low <= letter_count <= high:
            valid_count += 1
    
    return valid_count


if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        print(f"\n the file is {path}:")
        #print(os.getcwd())
        #if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/advent-of-code/2021/day-1/" + path
        #print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        info = parse(puzzle_input)
        print(calc_valid_passwords(info))
