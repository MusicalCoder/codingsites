import sys
import os

def parse(input):
    """Parse puzzle input"""
    return [int(line) for line in input]


def calc_entries(num_list):
    for num1 in num_list:
        for num2 in num_list:
            for num3 in num_list:
                if num1 > num2 > num3 and num1 + num2 + num3 == 2020:
                    return num1 * num2 * num3


if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        print(f"\n the file is {path}:")
        #print(os.getcwd())
        #if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/advent-of-code/2021/day-1/" + path
        #print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        numbers = parse(puzzle_input)
        print(calc_entries(numbers))
