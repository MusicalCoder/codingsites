import sys
import os

def parse(input):
    """Parse puzzle input"""    
    return input[0].strip()

def main_process(input):
    floor = 0
    for paren in input:
        if paren == "(":
            floor += 1
        else:
            floor -= 1
    
    return floor


if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        # print(f"\n the file is {path}:")
        # print(os.getcwd())
        # if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/advent-of-code/2021/day-3/" + path
        # print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        info = parse(puzzle_input)
        print(info)
        print(f"info is {len(info)} number of characters")
        print(main_process(info))
