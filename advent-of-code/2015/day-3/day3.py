import sys
import os

def parse(input):
    """Parse puzzle input"""    
    return input[0].strip()


def main_process(input):
    x = 0
    y = 0
    points = set()
    points.add((x, y))

    for item in input:
        if item == "^":
            y += 1
        elif item == ">":
            x += 1
        elif item == "v":
            y -= 1
        else:
            x -= 1

        points.add((x,y))
    
    return len(points)


if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        # print(f"\n the file is {path}:")
        # print(os.getcwd())
        # if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/advent-of-code/2015/day-2/" + path
        # print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        info = parse(puzzle_input)
        print(main_process(info))
