import sys
import os

def parse(input):
    """Parse puzzle input"""    
    dimension_list = []
    for entry in input:
        l, w, h = map(int, entry.strip().split("x"))
        dimension_list.append([l,w,h])
    
    return dimension_list


def main_process(input):
    total_ribbon = 0

    for item in input:
        new_list = sorted(item)
        ribbon = new_list[0] + new_list[0] + new_list[1] + new_list[1]
        bow = item[0] * item[1] * item[2]

        total_ribbon += ribbon + bow

    return total_ribbon


if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        # print(f"\n the file is {path}:")
        # print(os.getcwd())
        # if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/advent-of-code/2015/day-2/" + path
        # print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        info = parse(puzzle_input)
        print(main_process(info))
