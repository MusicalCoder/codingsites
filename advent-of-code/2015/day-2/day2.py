import sys
import os

def parse(input):
    """Parse puzzle input"""    
    dimension_list = []
    for entry in input:
        l, w, h = map(int, entry.strip().split("x"))
        dimension_list.append([l,w,h])
    
    return dimension_list


def main_process(input):
    wrapping_paper = 0

    for item in input:
        a1 = item[0] * item[1]
        a2 = item[1] * item[2]
        a3 = item[2] * item[0]

        wrapping_paper += min([a1, a2, a3]) + sum(list(map(lambda x: x * 2, [a1, a2, a3])))

    return wrapping_paper


if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        # print(f"\n the file is {path}:")
        # print(os.getcwd())
        # if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/advent-of-code/2015/day-2/" + path
        # print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        info = parse(puzzle_input)
        print(main_process(info))
