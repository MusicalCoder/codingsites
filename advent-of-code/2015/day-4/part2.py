import sys
import os

def parse(input):
    """Parse puzzle input"""    
    return input[0].strip()


def main_process(input):
    santa = (0, 0)
    bot = (0, 0)

    points = set()
    points.add(santa)

    for i, item in enumerate(input):
        if item == "^":
            pos = (0, 1)
        elif item == ">":
            pos = (1, 0)
        elif item == "v":
            pos = (0, -1)
        else:
            pos = (-1, 0)
        
        if i % 2:
            # bot moves
            bot = (pos[0]+bot[0], pos[1]+bot[1])
            points.add(bot)
        else:
            # santa moves
            santa = (pos[0]+santa[0], pos[1]+santa[1])
            points.add(santa)
    
    return len(points)


if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        # print(f"\n the file is {path}:")
        # print(os.getcwd())
        # if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/advent-of-code/2015/day-2/" + path
        # print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        info = parse(puzzle_input)
        print(main_process(info))
