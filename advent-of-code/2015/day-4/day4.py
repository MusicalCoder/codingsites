import sys
import os
import hashlib


def parse(input):
    """Parse puzzle input"""    
    return input[0].strip()


def main_process(input):
    number = 0
    result = "xxxxxxxxxx"

    while result[0:5] != "00000":
        number += 1
        hash_value = hashlib.md5(str(input + str(number)).encode())
        result = hash_value.hexdigest()
    
    print(result)
    return number

if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        # print(f"\n the file is {path}:")
        # print(os.getcwd())
        # if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/advent-of-code/2015/day-2/" + path
        # print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        info = parse(puzzle_input)
        print(main_process(info))
