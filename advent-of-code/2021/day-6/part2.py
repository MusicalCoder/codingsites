import sys
import os

num_days = 0

def parse(input):
    """Parse puzzle input"""    
    return [int(item) for item in input[0].split(",")]

def main_process(input):
    global num_days
    fish = [0,0,0,0,0,0,0,0,0]
    for i in input:
        fish[i] += 1

    for _ in range(num_days):
        new_adds = fish[0]      # anything at 0 becomes 6 and adds a new entry (8)
        for i in range(1, 9):
            fish[i-1] = fish[i]
            if i == 7:
                fish[i-1] += new_adds   # adding the resets
            if i == 8:
                fish[i] = new_adds  # creating the new fish

    return sum(fish)


if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        # print(f"\n the file is {path}:")
        # print(os.getcwd())
        # if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/advent-of-code/2021/day-6/" + path
        # print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        info = parse(puzzle_input)
        num_days = 18
        print(f"18 days has {main_process(info)} fish")
        num_days = 80
        print(f"while 80 days has {main_process(info)} fish...")
        num_days = 256
        print(f"while 256 days has {main_process(info)} fish...")
