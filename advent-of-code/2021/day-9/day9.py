import sys
import os

def parse(input):
    """Parse puzzle input"""    
    result = []
    for item in input:
        result.append([int(i) for i in item.strip()])
    return result


def main_process(input):
    low_points = []
    tp = 0
    lt = 0
    rt = len(input[0])
    bt = len(input)

    for r in range(bt):
        for c in range(rt):
            num = input[r][c]
            ahead = input[r][c+1] if c + 1 < rt else 99
            behind = input[r][c-1] if c - 1 >= lt else 99
            above = input[r-1][c] if r - 1 >= tp else 99
            below = input[r+1][c] if r + 1 < bt else 99
            if check_low_number(num, ahead, behind, above, below):
                low_points.append(input[r][c])
                # print(f"Added {input[r][c]} located at row {r+1} column {c+1}")
    
    return sum(low_points) + len(low_points)


def check_low_number(num, ahead, behind, above, below):
    if num >= ahead:
        return False
    if num >= behind:
        return False
    if num >= above:
        return False
    if num >= below:
        return False
    return True


if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        # print(f"\n the file is {path}:")
        # print(os.getcwd())
        # if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/2021/day-9/" + path
        # print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        info = parse(puzzle_input)
        print(main_process(info))
