import sys
import os

# low_points = []
borders = [0, 0, 0, 0]  #t, l, r, b

def parse(input):
    """Parse puzzle input"""    
    result = []
    for item in input:
        result.append([int(i) for i in item.strip()])
    return result


def main_process(input):
    global borders
    low_points = []
    borders = [0, 0, len(input[0]), len(input)]

    for r in range(borders[3]):
        for c in range(borders[2]):
            num = input[r][c]
            ahead = input[r][c+1] if c + 1 < borders[2] else 99
            behind = input[r][c-1] if c - 1 >= borders[1] else 99
            above = input[r-1][c] if r - 1 >= borders[0] else 99
            below = input[r+1][c] if r + 1 < borders[3] else 99
            if check_low_number(num, ahead, behind, above, below):
                low_points.append([[r, c], [input[r][c], 0]])
                # print(f"Added {input[r][c]} located at row {r+1} column {c+1}")
    
    # so now we have the low points...  from there we need to do basin checks
    max_basins = [0, 0, 0]
    for point in low_points:
        basin = set()
        basin.add((point[0][0], point[0][1]))
        x = 0
        if point[0][0] == 29 and point[0][1] == 63:
            x = 1
        map_basin(point, input, basin, x)
        if len(basin) > min(max_basins):
            max_basins.remove(min(max_basins))
            max_basins.append(len(basin))
            print(f"basin value of {len(basin)} starting at {point[0]}")
    
    return max_basins[0] * max_basins[1] * max_basins[2]
    

def map_basin(map, input, points, x):
    global borders
    test = map[1][0]
    value = 0
    r, c = map[0]

    above = input[r-1][c] if r - 1 >= borders[0] else 99
    left = input[r][c-1] if c - 1 >= borders[1] else 99
    right = input[r][c+1] if c + 1 < borders[2] else 99
    below = input[r+1][c] if r + 1 < borders[3] else 99

    if above > test and above < 9:
        points.add((r-1, c))
        map_basin([[r-1, c],[above, 0]], input, points, x)
    if left > test and left < 9:
        points.add((r, c-1))
        map_basin([[r, c-1],[left, 0]], input, points, x)
    if right > test and right < 9:
        points.add((r, c+1))
        map_basin([[r, c+1],[right, 0]], input, points, x)
    if below > test and below < 9:
        points.add((r+1, c))
        map_basin([[r+1, c],[below, 0]], input, points, x)
  

def check_low_number(num, ahead, behind, above, below):
    if num >= ahead:
        return False
    if num >= behind:
        return False
    if num >= above:
        return False
    if num >= below:
        return False
    return True


if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        # print(f"\n the file is {path}:")
        # print(os.getcwd())
        # if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/2021/day-9/" + path
        # print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        info = parse(puzzle_input)
        print(main_process(info))
