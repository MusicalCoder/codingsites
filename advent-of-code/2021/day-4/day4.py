import sys
import os


called_numbers = []

bingo_grid_list = []

class BingoGrid:
    # a bingo board is basically a 5 x 5 grid of numbers (so 25 cells)
    cells = []
    row_count = []
    col_count = []
    count_marked_cells = 0
    unmarked_total = 0

    def __init__(self) -> None:
        self.cells = [[[0, False], [0, False], [0, False], [0, False], [0, False]], 
            [[0, False], [0, False], [0, False], [0, False], [0, False]], 
            [[0, False], [0, False], [0, False], [0, False], [0, False]], 
            [[0, False], [0, False], [0, False], [0, False], [0, False]], 
            [[0, False], [0, False], [0, False], [0, False], [0, False]]]
        self.count_marked_cells = 0
        self.row_count = [0,0,0,0,0]
        self.col_count = [0,0,0,0,0]
        self.unmarked_total = 0
    
    def AddRowToGrid(self, index, values):
        for i, v in enumerate(values):
            self.cells[index][i][0] = int(v)

    def MarkCell(self, number):
        for i, row in enumerate(self.cells):
            for j, col in enumerate(row):
                if col[0] == number:
                    col[1] = True
                    self.count_marked_cells += 1
                    self.row_count[i] += 1
                    self.col_count[j] += 1
                    break

    def TestForBingo(self):
        if self.count_marked_cells < 5:
            return False

        for row in self.row_count:
            if row == 5:
                self.CalcUnmarkedTotal()
                return True

        for col in self.col_count:
            if col == 5:
                self.CalcUnmarkedTotal()
                return True

        return False

    def CalcUnmarkedTotal(self):
        for row in self.cells:
            for col in row:
                if not col[1]:
                    self.unmarked_total += col[0]


def parse(input):
    """Parse puzzle input"""    
    global called_numbers
    global bingo_grid_list

    row_count = 0
    grid_count = -1
    for i, line in enumerate(input):
        if i == 0:
            # line 1 is the called numnbers
            called_numbers = [int(item) for item in line.split(",")]
        else:
            if line not in ["\n", "\r\r"]:
                if row_count == 0:
                    # new set of rows for bingo - so add a new bingo grid to the list - and then add the row
                    grid_count += 1
                    bingo_grid_list.append(BingoGrid())

                bingo_grid_list[grid_count].AddRowToGrid(row_count, line. strip().split())
                row_count += 1
                if row_count >= 5:
                    row_count = 0


def main_process():
    global called_numbers
    global bingo_grid_list
    for ball in called_numbers:
        for board in bingo_grid_list:
            board.MarkCell(ball)
            
            if board.TestForBingo():
                return board.unmarked_total * ball


if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        # print(f"\n the file is {path}:")
        # print(os.getcwd())
        # if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/2021/day-4/" + path
        # print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        parse(puzzle_input)
        print(main_process())
