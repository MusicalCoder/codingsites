import sys
import os

def parse(input):
    """Parse puzzle input"""
    return [int(line) for line in input]


def count_increase(num_list):
    """Check num to see how many increase"""
    count = 0
    prev = 0

    for number in num_list:
        if prev != 0:
            if number > prev:
                count += 1

        prev = number

    return count        


if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        print(f"\n the file is {path}:")
        #print(os.getcwd())
        #if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/advent-of-code/2021/day-1/" + path
        #print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        numbers = parse(puzzle_input)
        print(count_increase(numbers))
