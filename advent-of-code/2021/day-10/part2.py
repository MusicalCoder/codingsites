import sys
import os

OPENCHARS = [40, 60, 91, 123]

POINTS = {
    "(": 1,
    "[": 2,
    "{": 3,
    "<": 4
}


def parse(input):
    """Parse puzzle input"""    
    return [item.strip() for item in input]


def main_process(input):
    incomplete = []
    for item in input:
        entry = []
        breakout = False
        for nav in item:
            if ord(nav) in OPENCHARS:
                entry.append(nav)
            else:
                if len(entry) == 0:
                    breakout = True
                    break
                else:
                    popped = entry.pop()
                    if (popped == "(" and nav != ")") or (popped == "<" and nav != ">") or (popped == "[" and nav != "]") or (popped == "{" and nav != "}"):
                        breakout = True
                        break
        if not breakout:
            incomplete.append(entry)
    
    scores = []
    global POINTS
    for entry in incomplete:
        score = 0
        while len(entry) > 0:
            score = score * 5 + int(POINTS.get(entry.pop()))
        scores.append(score)
    
    scores.sort()
    return scores[len(scores)//2]


if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        # print(f"\n the file is {path}:")
        # print(os.getcwd())
        # if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/2021/day-10/" + path
        # print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        info = parse(puzzle_input)
        print(main_process(info))
