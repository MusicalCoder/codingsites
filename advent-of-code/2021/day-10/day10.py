import sys
import os

OPENCHARS = [40, 60, 91, 123]
C_PAREN = 41
C_ANGLE = 62
C_BRACKET = 93
C_BRACE = 125


def parse(input):
    """Parse puzzle input"""    
    return [item.strip() for item in input]


def main_process(input):
    corrupted = []
    for item in input:
        entry = []
        for nav in item:
            if ord(nav) in OPENCHARS:
                entry.append(nav)
            else:
                if len(entry) == 0:
                    corrupted.append(nav)
                    break
                else:
                    popped = entry.pop()
                    if (popped == "(" and nav != ")") or (popped == "<" and nav != ">") or (popped == "[" and nav != "]") or (popped == "{" and nav != "}"):
                        corrupted.append(nav)
                        break

    # corrupted should contain only the corrupted lines
    result = 0
    for char in corrupted:
        if char == ")":
            result += 3
        elif char == "]":
            result += 57
        elif char == "}":
            result += 1197
        else:
            result += 25137
    
    return result


if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        # print(f"\n the file is {path}:")
        # print(os.getcwd())
        # if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/2021/day-9/" + path
        # print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        info = parse(puzzle_input)
        print(main_process(info))
