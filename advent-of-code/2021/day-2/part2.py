import sys
import os

def parse(input):
    """Parse puzzle input"""
    new_list = []
    for entry in input:
        direction, speed = entry.split(" ")
        new_list.append([direction, int(speed)])
    return new_list


def calculate_course(positions):
    horizontal = 0
    depth = 0
    aim = 0

    for position in positions:
        if position[0] == "forward":
            horizontal += position[1]
            depth += aim * position[1]
        elif position[0] == "down":
            aim += position[1]
        elif position[0] == "up":
            aim -= position[1]
    
    return horizontal * depth


if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        print(f"\n the file is {path}:")
        # print(os.getcwd())
        # if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/day-2/" + path
        # print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        positioning = parse(puzzle_input)
        print(calculate_course(positioning))
