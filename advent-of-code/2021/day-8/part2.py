import sys
import os


def parse(input):
    """Parse puzzle input"""    
    result = []
    for item in input:
        in_val, out_val = item.split(" | ")
        result.append([in_val, out_val])
    return result


def main_process(input):
    result = 0

    for item in input:
        digits = populate_digits(item[0])
        sequence = item[1].split()
        s_value = 1000 * digits["".join(sorted(sequence[0]))]
        s_value += 100 * digits["".join(sorted(sequence[1]))]
        s_value += 10 * digits["".join(sorted(sequence[2]))]
        s_value += digits["".join(sorted(sequence[3]))]
        result += s_value

    return result


def populate_digits(pattern):
    one = []
    four = []
    seven = []

    appearance = build_appearance()
    segments = build_segments()
    fivers = []
    sixers = []
    # item[0] is our input while [1] is our output
    for letter in pattern:
        if letter != " ":
            appearance[letter] += 1
    app_key_list = list(appearance.keys())
    app_val_list = list(appearance.values())
    found_values = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
    segments["f"] = app_key_list[app_val_list.index(9)]
    segments["e"] = app_key_list[app_val_list.index(4)]
    segments["b"] = app_key_list[app_val_list.index(6)]
    found_values.remove(segments["f"])
    found_values.remove(segments["e"])
    found_values.remove(segments["b"])

    sequence = [entry for entry in pattern.split()]
    for entry in sequence:
        if len(entry) == 2:  #one
            one = [c for c in entry]
        if len(entry) == 3: #seven
            seven = [c for c in entry]
        if len(entry) == 4: #four
            four = [c for c in entry]
        if len(entry) == 5:
            fivers.append(entry)
        if len(entry) == 6:
            sixers.append(entry)
    
    """
        okay - at this point - we have segments b, e, and f because they have unique counts
        c is the other segment in one (since f is in there)
        a is the other segment in seven (that isn't in one)
        d is the segment in four that isn't b, c, or f
        so thus the final segment (g) is the one missing letter
    """
    if one[0] == segments["f"]:
        segments["c"] = one[1]
        found_values.remove(one[1])
    elif one[1] == segments["f"]:
        segments["c"] = one[0]
        found_values.remove(one[0])
    else:
        print("PROBLEM WITH ONE")
        exit

    for letter in seven:
        if letter not in one:
            segments["a"] = letter
            found_values.remove(letter)
    
    for letter in four:
        if letter not in one:
            if letter != segments["b"]:
                segments["d"] = letter
                found_values.remove(letter)
    
    if len(found_values) != 1:
        print("PROBLEM FINDING SEGMENT VALUES")
        exit
    
    segments["g"] = found_values[0]

    # ok - segments now has the letter values that make up our numbers
    return build_digits(segments)


def build_appearance():
    return { "a": 0, "b": 0, "c": 0, "d": 0, "e": 0, "f": 0, "g": 0 }


def build_segments():
    return { "a": "", "b": "", "c": "", "d": "", "e": "", "f": "", "g": "" }


def build_digits(segments):
    temp = {}
    temp["".join(sorted(segments["a"] + segments["b"] + segments["c"] + segments["e"] + segments["f"] + segments["g"]))] = 0
    temp["".join(sorted(segments["c"] + segments["f"]))] = 1
    temp["".join(sorted(segments["a"] + segments["c"] + segments["d"] + segments["e"] + segments["g"]))] = 2
    temp["".join(sorted(segments["a"] + segments["c"] + segments["d"] + segments["f"] + segments["g"]))] = 3
    temp["".join(sorted(segments["b"] + segments["c"] + segments["d"] + segments["f"]))] = 4
    temp["".join(sorted(segments["a"] + segments["b"] + segments["d"] + segments["f"] + segments["g"]))] = 5
    temp["".join(sorted(segments["a"] + segments["b"] + segments["d"] + segments["e"] + segments["f"] + segments["g"]))] = 6
    temp["".join(sorted(segments["a"] + segments["c"] + segments["f"]))] = 7
    temp["".join(sorted(segments["a"] + segments["b"] + segments["c"] + segments["d"] + segments["e"] + segments["f"] + segments["g"]))] = 8
    temp["".join(sorted(segments["a"] + segments["b"] + segments["c"] + segments["d"] + segments["f"] + segments["g"]))] = 9
    return temp


if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        # print(f"\n the file is {path}:")
        # print(os.getcwd())
        # if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/2021/day-8/" + path
        # print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        info = parse(puzzle_input)
        print(main_process(info))
