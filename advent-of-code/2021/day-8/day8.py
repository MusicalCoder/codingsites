import sys
import os

def parse(input):
    """Parse puzzle input"""    
    result = []
    for item in input:
        in_val, out_val = item.split(" | ")
        result.append([in_val, out_val])
    return result


def main_process(input):
    result = 0
    for item in input:
        values = item[1].split()
        for entry in values:
            if len(entry) in [2, 3, 4, 7]:
                result += 1
    
    return result


if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        # print(f"\n the file is {path}:")
        # print(os.getcwd())
        # if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/2021/day-8/" + path
        # print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        info = parse(puzzle_input)
        print(main_process(info))
