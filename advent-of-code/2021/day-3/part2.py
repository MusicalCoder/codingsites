import sys
import os

binary_length = 0

def parse(input):
    """Parse puzzle input"""    
    return [line.strip() for line in input]

def main_process(input):
    global binary_length
    oxygen_list = list(input)
    co2_list = list(input)

    for i in range(binary_length):
        zeros = 0
        ones = 0

        for entry in oxygen_list:
            if entry[i] == "0":
                zeros += 1
            else:
                ones += 1
        
        if zeros > ones:
            oxygen_list = [item for item in oxygen_list if item[i] == "0"]
        else:
            oxygen_list = [item for item in oxygen_list if item[i] == "1"]

        if len(oxygen_list) == 1:
            break
    
    for i in range(binary_length):
        zeros = 0
        ones = 0

        for entry in co2_list:
            if entry[i] == "0":
                zeros += 1
            else:
                ones += 1
        
        if zeros > ones:
            co2_list = [item for item in co2_list if item[i] == "1"]
        else:
            co2_list = [item for item in co2_list if item[i] == "0"]

        if len(co2_list) == 1:
            break
    
    print(f"oxygen = {oxygen_list[0]} ({int(oxygen_list[0], 2)}) and co2 = {co2_list[0]} ({int(co2_list[0], 2)})")
    return int(oxygen_list[0], 2) * int(co2_list[0], 2)


if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        # print(f"\n the file is {path}:")
        # print(os.getcwd())
        # if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/advent-of-code/2021/day-3/" + path
        # print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        info = parse(puzzle_input)
        binary_length = len(info[0])
        print(main_process(info))
