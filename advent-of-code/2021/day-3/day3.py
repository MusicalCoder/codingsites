import sys
import os

binary_length = 0

def parse(input):
    """Parse puzzle input"""    
    return [line.strip() for line in input]

def main_process(input):
    global binary_length
    binary_values = []
    gamma = ""
    epsilon = ""

    for i in range(binary_length):
        binary_values.append([0,0])

    for entry in input:
        for i, v in enumerate(entry):
            if v == "0":
                binary_values[i][0] += 1
            else:
                binary_values[i][1] += 1
    
    for item in binary_values:
        if item[0] > item[1]:
            gamma += "0"
            epsilon += "1"
        else:
            gamma += "1"
            epsilon += "0"

    print(f"gamma = {gamma} ({int(gamma, 2)}) and epsilon = {epsilon} ({int(epsilon, 2)})")
    return int(gamma, 2) * int(epsilon, 2)


if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        # print(f"\n the file is {path}:")
        # print(os.getcwd())
        # if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/advent-of-code/2021/day-3/" + path
        # print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        info = parse(puzzle_input)
        binary_length = len(info[0])
        print(main_process(info))
