import sys
import os


STEP_COUNT = 0
FLASH_COUNT = 0
FLASHERS = []
GRID = []
MAX_ROW = 0
MAX_COLUMN = 0


def parse(input):
    global GRID
    """Parse puzzle input"""    
    for entry in input:
        GRID.append([int(item) for item in entry.strip()])


def main_process():
    global GRID
    global FLASHERS
    global FLASH_COUNT
    global MAX_ROW
    global MAX_COLUMN

    step = 0
    while FLASH_COUNT < (MAX_COLUMN * MAX_ROW):
        step += 1
        FLASH_COUNT = 0
        increment_grid()
        test_flash()
        current = 0
        while current != len(FLASHERS):
            current = len(FLASHERS)
            flash()
            test_flash()
        reset_flash()
        #print(f"Flashes after step {_ + 1} : {cur_flash}")
        #print(*GRID, sep = "\n")
    return step
    
        

def increment_grid():
    global GRID
    for r, val in enumerate(GRID):
        for c, v in enumerate(GRID[r]):
            GRID[r][c] += 1


def test_flash():
    global GRID
    global FLASHERS
    for r, _ in enumerate(GRID):
        for c, _ in enumerate(GRID[r]):
            if GRID[r][c] > 9:
                found = False
                for x, y in FLASHERS:
                    if (r, c) == x:
                        found = True
                        break
                if not found:
                    FLASHERS.append([(r, c), False])


def flash():
    global GRID
    global FLASHERS
    global MAX_ROW
    global MAX_COLUMN

    for idx, value in enumerate(FLASHERS):
        r, c = value[0]
        if not value[1]:
            if r - 1 >= 0:
                if c - 1 >= 0:
                    GRID[r-1][c-1] += 1
                GRID[r-1][c] += 1
                if c + 1 < MAX_COLUMN:
                    GRID[r-1][c+1] += 1
            if c - 1 >= 0:
                GRID[r][c-1] += 1
            GRID[r][c] += 1
            if c + 1 < MAX_COLUMN:
                GRID[r][c+1] += 1
            if r + 1 < MAX_ROW:
                if c - 1 >= 0:
                    GRID[r+1][c-1] += 1
                GRID[r+1][c] += 1
                if c + 1 < MAX_COLUMN:
                    GRID[r+1][c+1] += 1
            FLASHERS[idx][1] = True


def reset_flash():
    global GRID
    global FLASHERS
    global FLASH_COUNT
    global MAX_ROW
    global MAX_COLUMN

    for r in range(MAX_ROW):
        for c in range(MAX_COLUMN):
            if GRID[r][c] > 9:
                GRID[r][c] = 0
                FLASH_COUNT += 1
    FLASHERS = list()


if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        # print(f"\n the file is {path}:")
        # print(os.getcwd())
        # if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/2021/day-11/" + path
        # print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        parse(puzzle_input)
        MAX_ROW = len(GRID)
        MAX_COLUMN = len(GRID[0])
        STEP_COUNT = 100

        print(main_process())
        #print(FLASH_COUNT)
