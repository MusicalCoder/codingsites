import sys
import os

def parse(input):
    """Parse puzzle input"""    
    return [int(item) for item in input[0].split(",")]


def main_process(input):
    mid_value = sum(input) // len(input)

    # mid_value is one of the center most values in the list - and should be the economic fuel point
    fuel = 0
    for item in input:
        fuel += sum_of_digits(abs(item - mid_value))

    return fuel


def sum_of_digits(value):
    return (value * (value + 1)) // 2


if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        # print(f"\n the file is {path}:")
        # print(os.getcwd())
        # if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/2021/day-7/" + path
        # print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        info = parse(puzzle_input)
        print(main_process(info))
