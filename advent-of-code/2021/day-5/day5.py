import sys
import os

max_point = 0

def parse(input):
    """Parse puzzle input"""    
    global max_point
    result = []
    for line in input:
        points = line.strip().split(" -> ")
        result.append(points)
        for i in range(2):
            p1, p2 = map(int, points[i].split(","))
            max_point = max(max_point, p1, p2)
    return result


def main_process(input):
    point_grid = build_point_grid()
    for points in input:
        # to make things simpler to follow we want to 'draw' the line always from the furthest point out either down or out
        # so we'll use left and right to start with, and then if necessary, flop them from start to end... where start is the closest
        # to 0,0 and end is the closest to 9,9 - the idea is we will either be adding to x or adding to y - never subtracting 
        left, right = points
        leftx, lefty = map(int, left.split(","))
        rightx, righty = map(int, right.split(","))
        if leftx == rightx or lefty == righty:
            # only a horizontal or vertical line
            if leftx > rightx or lefty > righty:
                sx, sy = map(int, right.split(","))
                ex, ey = map(int, left.split(","))
            else:
                sx, sy = map(int, left.split(","))
                ex, ey = map(int, right.split(","))

            point_grid[sy][sx] += 1
            if sx != ex:
                # x values differ so our line moves horizontally
                while sx != ex:
                    sx += 1
                    point_grid[sy][sx] += 1
            else:
                # y values differ so our line moves vertically
                while sy != ey:
                    sy += 1
                    point_grid[sy][sx] += 1
            
    # our grid should now be built
    dangerous = 0
    for sector in point_grid:
        dangerous += sum(i > 1 for i in sector)
    
    return dangerous


def build_point_grid():
    global max_point
    max_point += 1
    result = []
    for _ in range(max_point):
        result.append([0]*max_point)
    return result


if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        # print(f"\n the file is {path}:")
        # print(os.getcwd())
        # if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/advent-of-code/2021/day-5/" + path
        # print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        info = parse(puzzle_input)
        print(main_process(info))
