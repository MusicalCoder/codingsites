import sys
import os

max_point = 0

def parse(input):
    """Parse puzzle input"""    
    global max_point
    result = []
    for line in input:
        points = line.strip().split(" -> ")
        result.append(points)
        for i in range(2):
            p1, p2 = map(int, points[i].split(","))
            max_point = max(max_point, p1, p2)
    return result


def main_process(input):
    point_grid = build_point_grid()
    for points in input:
        start, end = points
        sx, sy = map(int, start.split(","))
        ex, ey = map(int, end.split(","))

        point_grid[sy][sx] += 1
        while sx != ex or sy != ey:
            newpt = next_point([sx, sy], [ex, ey])
            sx, sy = newpt[0], newpt[1]
            point_grid[sy][sx] += 1
            
    # our grid should now be built
    dangerous = 0
    for sector in point_grid:
        dangerous += sum(i > 1 for i in sector)
    
    return dangerous


def next_point(curpt, endpt):
    # input is 2 lists, consisting each of 2 integers separated by a comma.  ie: [101,244]... etc
    cx, cy = curpt[0], curpt[1]
    ex, ey = endpt[0], endpt[1]
    if cx < ex:
        if cy < ey:
            return [cx+1, cy+1]
        if cy == ey:
            return [cx+1, cy]
        if cy > ey:
            return [cx+1, cy-1]
    elif cx == ex:
        if cy < ey:
            return [cx, cy+1]
        if cy > ey:
            return [cx, cy-1]
    else:
        if cy < ey:
            return [cx-1, cy+1]
        if cy == ey:
            return [cx-1, cy]
        if cy > ey:
            return [cx-1, cy-1]
    

def build_point_grid():
    global max_point
    max_point += 1
    result = []
    for _ in range(max_point):
        result.append([0]*max_point)
    return result


if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        # print(f"\n the file is {path}:")
        # print(os.getcwd())
        # if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/advent-of-code/2021/day-5/" + path
        # print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        info = parse(puzzle_input)
        print(main_process(info))
