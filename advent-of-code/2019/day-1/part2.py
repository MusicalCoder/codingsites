import sys
import os

def parse(input):
    """Parse puzzle input"""
    return [int(item) for item in input]


def calc_fuel_reqs(input):
    total = 0

    for mass in input:
        fuel_need = mass // 3 - 2
        total += fuel_need

        while fuel_need > 0:
            fuel_need = fuel_need // 3 - 2
            total += fuel_need if fuel_need > 0 else 0
                
    return total


if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        print(f"\n the file is {path}:")
        #print(os.getcwd())
        #if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/advent-of-code/2021/day-1/" + path
        #print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        info = parse(puzzle_input)
        print(calc_fuel_reqs(info))
