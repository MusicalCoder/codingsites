import sys
import os

def parse(input):
    """Parse puzzle input"""
    return [int(item) for item in input[0].split(",")]

def process_opcode(input):
    idx = 0
    continue_process = True
    while continue_process:
        command = input[idx]
        left_loc = input[idx+1]
        right_loc = input[idx+2]
        result_loc = input[idx+3]

        if command == 99:
            continue_process = False
        elif command == 1:
            add1 = input[left_loc]
            add2 = input[right_loc]
            input[result_loc] = add1 + add2
        elif command == 2:
            mult1 = input[left_loc]
            mult2 = input[right_loc]
            input[result_loc] = mult1 * mult2
        else:
            print("Invalid command - something went wrong")
            continue_process = False
        
        idx += 4

    return input[0]


def find_proper_output(input):
    for noun in range(100):
        for verb in range(100):
            opcode = list(input)
            opcode[1] = noun
            opcode[2] = verb
            result = process_opcode(opcode)

            if result == 19690720:
                return (100 * noun) + verb

    print("value not found")        
    return 0


if __name__ == "__main__":
    if sys.argv[1] != None:
        path = sys.argv[1]
        # print(f"\n the file is {path}:")
        # print(os.getcwd())
        # if os.getcwd()[-4] != ".txt":
        #    path = os.getcwd() + "/2019/day-2/" + path
        # print(f"\n the new file is {path}:")

        puzzle_input = open(path, 'r').readlines()

        info = parse(puzzle_input)
        print(find_proper_output(info))
